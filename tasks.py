# coding=utf-8

from invoke import task


@task
def pep8(ctx):
  ctx.run("pycodestyle django_competition django_competition_test")


@task
def lint(ctx):
  ctx.run("pylint django_competition django_competition_test -r n")


@task
def test(ctx):
  ctx.run("py.test -v --cov django_competition --cov-report=html --cov-report=term-missing django_competition_test")


@task(pre=[test, pep8, lint])
def check(ctx):
  pass




