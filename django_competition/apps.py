"""Apps module."""
from django.apps import AppConfig


class DjangoCompetitionConfig(AppConfig):
  """Apps class."""
  name = 'django_competition'
