Django competition
==================

Django package to perform voting for competitions.

Notable features:

1. Competitions are defined inside Django Administrative interface;
2. You can customize number of votes per day, and number of votes per user;
3. To vote user needs to click on link inside an e-mail;
4. You can disallow disposable e-mail providers
   (using https://github.com/FGRibreau/mailchecker).
5. Vote e-mails are stored to diagnose any rule breaking.
6. We are GDPR friendly as e-mail is stored in our system only after user
   confirm it's validity.

Template overriding:

Overriding ``django_competiton/partials/base.html`` should give you a good
start (it is a parent for all other templates) should give you a good start.

This project has received funding from the European Union Horizon 2020 Research
and Innovation programme `under grant agreement no. 710577.
<https://cordis.europa.eu/project/rcn/203170_en.html>`__.
